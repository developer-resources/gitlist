# GitList: an elegant git repository viewer
[![Build Status](https://travis-ci.org/andrewjcurrie/gitlist.svg)](https://travis-ci.org/andrewjcurrie/gitlist)

GitList is an elegant and modern web interface for interacting with multiple git repositories. It allows you to browse repositories using your favorite browser, viewing files under different revisions, commit history, diffs. It also generates RSS feeds for each repository, allowing you to stay up-to-date with the latest changes anytime, anywhere. GitList was written in PHP, on top of the [Silex](http://silex.sensiolabs.org/) microframework and powered by the Twig template engine. This means that GitList is easy to install and easy to customize. The gorgeous GitList interface was made possible due to [Bootstrap](http://twitter.github.com/bootstrap/). 

## Features
* Multiple repository support
* Multiple branch support
* Multiple tag support
* Commit history, blame, diff
* RSS feeds
* Syntax highlighting
* Repository statistics

## Screenshots
[![GitList Screenshot](https://digitalpci.com/wp-content/themes/digitalpci/images/projects-thumb.jpg)](https://digitalpci.com/wp-content/themes/digitalpci/images/projects.jpg)
[![GitList Screenshot](https://digitalpci.com/wp-content/themes/digitalpci/images/commits-thumb.jpg)](https://digitalpci.com/wp-content/themes/digitalpci/images/commits.jpg)

## Requirements
In order to run GitList on your server, you'll need:

* Git
* Apache with mod_rewrite enabled or NGINX
* PHP 5.3.3+

## Installation
* Download GitList from [gitlist.org](http://gitlist.org/) and decompress to your `/var/www/gitlist` folder, or anywhere else you want to place GitList. 
* Do not download a branch or tag from GitHub, unless you want to use the development version. The version available for download at the website already has all dependencies bundled, so you don't have to use composer or any other tool
* Rename the `config.ini-example` file to `config.ini`.
* Open up the `config.ini` and configure your installation. You'll have to provide where your repositories are located.
* In case GitList isn't accessed through the root of the website, open .htaccess and edit RewriteBase (for example, /gitlist/ if GitList is accessed through http://localhost/gitlist/).
* Create the cache folder and give read/write permissions to your web server user:

```
cd /var/www/gitlist
mkdir cache
chmod 777 cache
```
## Authors and contributors
* [Klaus Silveira](http://www.klaussilveira.com) (Creator, developer)

## License
[New BSD license](http://www.opensource.org/licenses/bsd-license.php)

## Todo
* add user accounts
* add the ability to mark a repo private or public
